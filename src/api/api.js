/*
This code needs to be ready to be merged
You can do all the modification you want
 
r.perform() -> returns a boolean (true if the request is successful, false otherwise)
 
'get', 'post' and 'put' are used in the project, they must be defined as follow:
    get(url, params)
    post(url, params)
    put(url, params)


Don't hesitate to ask questions and / or explain what's your train of thoughts
*/

function get(url, params) {
  var r = Request.new(url, params);
  r.setMethod("GET");

  if (r.perform()) {
    return true;
  } else {
    return false;
  }
}

function post(url, params) {
  var r = Request.new(url, params);
  r.setMethod("POST");

  if (r.perform()) {
    return true;
  } else {
    return false;
  }
}
