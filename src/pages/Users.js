import React from "react";
import "./Users.css";

const Users = () => {
  return (
    <div className="Users">
      Use the following JSON endpoint to create a list of users with their name
      and email addresses
      <p className="code">https://jsonplaceholder.typicode.com/users</p>
      <p>Ask as many question as you want, search for info online if needed</p>
    </div>
  );
};

export default Users;
