import revolin from "./revolin.png";
import "./App.css";
import { Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={revolin} className="App-logo" alt="logo" />
        <p>Welcome to revolin JS/React coding test</p>
        <h2>To Do:</h2>
        <p>
          We'll need an API for our project, check
          <span className="code">api/api.js</span>
          and update it
        </p>
        <p>
          When you're done with improving our API, follow instructions on{" "}
          <Link to="/users">this page</Link>
        </p>
      </header>
    </div>
  );
}

export default App;
